# Tugas 03
***

## Subquery
1. **Buatlah list pegawai pada departemen selain sales (id=80) yang memiliki gaji lebih besar dari gaji tertinggi pegawai yang memiliki nomor telepon diawali dengan angka 6.**
<pre><code>
SELECT * FROM EMPLOYEES
WHERE SALARY &gt (SELECT MAX(SALARY) FROM EMPLOYEES WHERE SUBSTRING(PHONE_NUMBER, 1,1)=6)AND DEPARTMENT_ID != 80
</pre></code>
**Output :**
| EMPLOYEE\_ID | FIRST\_NAME | LAST\_NAME | EMAIL    | PHONE\_NUMBER | HIRE\_DATE | JOB\_ID  | SALARY | COMMISSION\_PCT | MANAGER\_ID | DEPARTMENT\_ID |
| ------------ | ----------- | ---------- | -------- | ------------- | ---------- | -------- | ------ | --------------- | ----------- | -------------- |
| 100          | Steven      | King       | SKING    | 515.123.4567  | 17/06/2003 | AD\_PRES | 24000  | NULL            | NULL        | 90             |
| 101          | Neena       | Kochhar    | NKOCHHAR | 515.123.4568  | 21/09/2005 | AD\_VP   | 17000  | NULL            | 100         | 90             |
| 102          | Lex         | De Haan    | LDEHAAN  | 515.123.4569  | 13/01/2001 | AD\_VP   | 17000  | NULL            | 100         | 90             |
| 108          | Nancy       | Greenberg  | NGREENBE | 515.124.4569  | 17/08/2002 | FI\_MGR  | 12008  | NULL            | 101         | 100            |
| 114          | Den         | Raphaely   | DRAPHEAL | 515.127.4561  | 07/12/2002 | PU\_MAN  | 11000  | NULL            | 100         | 30             |
| 201          | Michael     | Hartstein  | MHARTSTE | 515.123.5555  | 17/02/2004 | MK\_MAN  | 13000  | NULL            | 100         | 20             |
| 204          | Hermann     | Baer       | HBAER    | 515.123.8888  | 07/06/2002 | PR\_REP  | 10000  | NULL            | 101         | 70             |
| 205          | Shelley     | Higgins    | SHIGGINS | 515.123.8080  | 07/06/2002 | AC\_MGR  | 12008  | NULL            | 101         | 110            |

2. **Buatlah laporan departemen-departemen dengan total gaji pegawai perbulan dibawah besarnya gaji pegawai paling tinggi. Urutkan dengan mulai dari departemen dengan total gaji paling kecil.**
<pre><code>
SELECT DEPARTMENT_ID, SUM (SALARY) AS TOTAL_GAJI
FROM EMPLOYEES
GROUP BY DEPARTMENT_ID
HAVING SUM(SALARY) &tl (SELECT MAX(SALARY)FROM EMPLOYEES)
ORDER BY TOTAL_GAJI DESC
</pre></code>
**Output :**
| DEPARTMENT\_ID | TOTAL\_GAJI |
| -------------- | ----------- |
| 110            | 20308       |
| 20             | 19000       |
| 70             | 10000       |
| NULL           | 7000        |
| 40             | 6500        |
| 10             | 4400        |

3. **Buatlah daftar informasi jumlah pegawai per masing-masing departemen yang berlokasi pada wilayah dengan location_id = 1700 dengan format seperti di bawah ini.**
<pre><code>
SELECT DEPARTMENT_ID, COUNT(EMPLOYEE_ID) AS NUMBER_OF_EMPLOYEES FROM EMPLOYEES 
WHERE DEPARTMENT_ID IN (SELECT DEPARTMENT_ID FROM DEPARTMENTS WHERE LOCATION_ID=1700)
GROUP BY DEPARTMENT_ID
</pre></code>
**Output:**
| DEPARTMENT\_ID | NUMBER\_OF\_EMPLOYEES |
| -------------- | --------------------- |
| 10             | 1                     |
| 30             | 6                     |
| 90             | 3                     |
| 100            | 6                     |
| 110            | 2                     |

4. **Cari setiap pegawai yang memiliki gaji lebih besar dari semua pegawai rekan kerja Alexander Hunold pada satu department yang sama, urutkan dari gaji yang terbesar.**
<pre><code>
SELECT EMPLOYEE_ID, FIRST_NAME, LAST_NAME, SALARY  FROM EMPLOYEES
WHERE SALARY > 
(SELECT MAX (SALARY) FROM EMPLOYEES  WHERE DEPARTMENT_ID= (SELECT DEPARTMENT_ID FROM EMPLOYEES 
WHERE FIRST_NAME='Alexander' and LAST_NAME='Hunold') and (FIRST_NAME!='Alexander' and LAST_NAME!='Hunold'))
ORDER BY SALARY DESC
</pre></code>
**Output :** 56 rows
| EMPLOYEE\_ID | FIRST\_NAME | LAST\_NAME | SALARY |
| ------------ | ----------- | ---------- | ------ |
| 100          | Steven      | King       | 24000  |
| 101          | Neena       | Kochhar    | 17000  |
| 102          | Lex         | De Haan    | 17000  |
| 145          | John        | Russell    | 14000  |
| …            | …           | …          | …      |
| 166          | Sundar      | Ande       | 6400   |
| 167          | Amit        | Banda      | 6200   |
| 179          | Charles     | Johnson    | 6200   |
| 173          | Sundita     | Kumar      | 6100   |

5. **Cari pegawai yang memiliki gaji yang berada pada range gaji pegawai yang dipersyaratkan Di jabatan Accountant (FI_ACCOUNT).**
<pre><code>
SELECT*FROM EMPLOYEES 
WHERE SALARY >  (SELECT MIN_SALARY FROM JOBS WHERE JOB_ID='FI_ACCOUNT') 
AND SALARY < (SELECT MAX_SALARY FROM JOBS WHERE JOB_ID='FI_ACCOUNT')
</pre></code>
**Output :**
| EMPLOYEE\_ID | FIRST\_NAME | LAST\_NAME | EMAIL    | PHONE\_NUMBER | HIRE\_DATE | JOB\_ID     | SALARY | COMMISSION\_PCT | MANAGER\_ID | DEPARTMENT\_ID |
| ------------ | ----------- | ---------- | -------- | ------------- | ---------- | ----------- | ------ | --------------- | ----------- | -------------- |
| 104          | Bruce       | Ernst      | BERNST   | 590.423.4568  | 21/05/2007 | IT\_PROG    | 6000   | NULL            | 103         | 60             |
| 105          | David       | Austin     | DAUSTIN  | 590.423.4569  | 25/06/2005 | IT\_PROG    | 4800   | NULL            | 103         | 60             |
| 106          | Valli       | Pataballa  | VPATABAL | 590.423.4560  | 05/02/2006 | DB\_ADMIN   | 4800   | NULL            | 103         | 60             |
| …            | …           | …          | …        | …             | …          | …           | …      | …               | …           | …              |
| 202          | Pat         | Fay        | PFAY     | 603.123.6666  | 17/08/2005 | MK\_REP     | 6000   | NULL            | 201         | 20             |
| 203          | Susan       | Mavris     | SMAVRIS  | 515.123.7777  | 07/06/2002 | HR\_REP     | 6500   | NULL            | 101         | 40             |
| 206          | William     | Gietz      | WGIETZ   | 515.123.8181  | 07/06/2002 | AC\_ACCOUNT | 8300   | NULL            | 205         | 110            |