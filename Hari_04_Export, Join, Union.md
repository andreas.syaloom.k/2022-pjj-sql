# Tugas 01
***
1. Lakukan ekspor data untuk case latihan sebelumnya di hari ini
2. Cari 10 pegawai dengan masa kerja paling lama. Buat dalam file csv dengan delimiter
3. Cari Jumlah pegawai per Job_ID. Buat dalam file excel 
<pre><code>
select job_id, count(*) as total_pegawai from EMPLOYEES
group by job_id
</pre></code>

**Output:**
| job\_id     | total\_pegawai |
| ----------- | -------------- |
| AC\_ACCOUNT | 1              |
| AC\_MGR     | 1              |
| AD\_ASST    | 1              |
| AD\_PRES    | 1              |
| AD\_VP      | 2              |
| DB\_ADMIN   | 1              |
| FI\_ACCOUNT | 5              |
| FI\_MGR     | 1              |
| HR\_REP     | 1              |
| IT\_PROG    | 4              |
| MK\_MAN     | 1              |
| MK\_REP     | 1              |
| PR\_REP     | 1              |
| PU\_CLERK   | 5              |
| PU\_MAN     | 1              |
| SA\_MAN     | 5              |
| SA\_REP     | 30             |
| SH\_CLERK   | 20             |
| ST\_CLERK   | 20             |

# Tugas 02
*** 
1. **Carilah pegawai yang mendapatkan gaji bulanan kurang dari standar yang ditentukan oleh HR!**
* Tampilan Employee_Id, Name, Month, Year, Salary, Salary_real, Description.

<pre><code>USE FINANCES
GO

SELECT pay.employee_id, emp.first_name, emp.last_name, pay.month_paid, pay.year_paid, pay.salary_real, emp.salary, pay.dscription
FROM PAYROLL pay  
LEFT JOIN tSQL.dbo.employees emp ON pay.employee_id=emp.employee_id 
WHERE salary_real < salary
</pre></code>
**Output :**
| employee\_id | first\_name | last\_name | month\_paid | year\_paid | salary\_real | salary | dscription                                |
| ------------ | ----------- | ---------- | ----------- | ---------- | ------------ | ------ | ----------------------------------------- |
| 130          | Mozhe       | Atkinson   | 1           | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 2           | 2020       | 2800         | 99000  |                                           |
| 176          | Jonathon    | Taylor     | 2           | 2020       | 0            | 8600   | Cuti Besar. surat cuti nomer S-51/HR/2020 |
| 190          | Timothy     | Gates      | 2           | 2020       | 900          | 2900   |                                           |
| 130          | Mozhe       | Atkinson   | 3           | 2020       | 2800         | 99000  |                                           |
| 156          | Janette     | King       | 3           | 2020       | 9000         | 10000  | Potongan absen 10%.                       |
| 120          | Matthew     | Weiss      | 4           | 2020       | 6800         | 8000   | Potongan absen 15%.                       |
| 130          | Mozhe       | Atkinson   | 4           | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 6           | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 7           | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 8           | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 9           | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 10          | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 11          | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 12          | 2020       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 1           | 2021       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 2           | 2021       | 2800         | 99000  |                                           |
| 130          | Mozhe       | Atkinson   | 4           | 2021       | 2800         | 99000  |                                           |

2. **Carilah pegawai yang mendapatkan gaji bulanan kurang dari standar yang ditentukan oleh HR namun tidak memiliki alasan!**
* Tampilan Employee_Id, Name, Month, Year, Salary, Salary_real, Description
* Hint! GUNAKAN LEN()
<pre><code>
USE FINANCES
GO

SELECT pay.employee_id, emp.first_name, emp.last_name, pay.month_paid, pay.year_paid, pay.salary_real, emp.salary, pay.dscription
FROM PAYROLL pay  
LEFT JOIN tSQL.dbo.employees emp ON pay.employee_id=emp.employee_id 
WHERE salary_real < salary and len(dscription)=0
</pre></code>
**Output :**
| employee\_id | first\_name | last\_name | month\_paid | year\_paid | salary\_real | salary | dscription |
| ------------ | ----------- | ---------- | ----------- | ---------- | ------------ | ------ | ---------- |
| 130          | Mozhe       | Atkinson   | 1           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 2           | 2020       | 2800         | 99000  |            |
| 190          | Timothy     | Gates      | 2           | 2020       | 900          | 2900   |            |
| 130          | Mozhe       | Atkinson   | 3           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 4           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 6           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 7           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 8           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 9           | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 10          | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 11          | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 12          | 2020       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 1           | 2021       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 2           | 2021       | 2800         | 99000  |            |
| 130          | Mozhe       | Atkinson   | 4           | 2021       | 2800         | 99000  |            |

3. **Carilah pegawai yang tidak mendapatkan Gaji dan tidak terdaftar pada table payroll pada bulan 5 tahun 2020!**
* Tampilan Employee_Id, Name, Month, Year, Salary, Salary_real, Description)
* Hint! GUNAKAN SUBQUERY
<pre><code>
USE FINANCES
GO

SELECT e.EMPLOYEE_ID, CONCAT(e.FIRST_NAME, ' ', e.LAST_NAME) AS Name, p.MONTH_PAID, p.YEAR_PAID, e.SALARY, p.SALARY_REAL 
FROM (SELECT * FROM FINANCES.dbo.PAYROLL WHERE MONTH_PAID=5 AND YEAR_PAID=2020) p
RIGHT JOIN tSQL.dbo.EMPLOYEES as e
ON p.EMPLOYEE_ID=e.EMPLOYEE_ID
WHERE p.SALARY_REAL IS NULL
</pre></code>
**Output:**
| EMPLOYEE\_ID | Name           | MONTH\_PAID | YEAR\_PAID | SALARY | SALARY\_REAL |
| ------------ | -------------- | ----------- | ---------- | ------ | ------------ |
| 130          | Mozhe Atkinson | NULL        | NULL       | 99000  | NULL         |
| 206          | William Gietz  | NULL        | NULL       | 8300   | NULL         |

4. **Temukan bulan yang lompat pada table payroll!**
* Tampilan Month, Year)
* Hint! GUNAKAN GROUP BY
<pre><code>
SELECT YEAR_PAID, MONTH_PAID FROM PAYROLL
GROUP BY YEAR_PAID, MONTH_PAID
ORDER BY YEAR_PAID, MONTH_PAID
</pre></code>
**Output :**
| YEAR\_PAID | MONTH\_PAID |
| ---------- | ----------- |
| 2020       | 1           |
| 2020       | 2           |
| 2020       | 3           |
| 2020       | 4           |
| 2020       | 5           |
| 2020       | 6           |
| 2020       | 7           |
| 2020       | 8           |
| 2020       | 9           |
| 2020       | 10          |
| 2020       | 11          |
| 2020       | 12          |
| 2021       | 1           |
| 2021       | 2           |
| 2021       | 4           |

5. **Temukan pegawai yang mendapatkan kenaikan gaji, namun datanya belum diperbarui pada table HR!** 
Tampilan Employee_Id, Name, Month, Year, Salary, Salary_real, Description<br />
<pre><code>
USE tSQL
GO

SELECT p.EMPLOYEE_ID, CONCAT(e.FIRST_NAME, ' ', e.LAST_NAME) AS Name, p.MONTH_PAID, p.YEAR_PAID, e.SALARY, p.SALARY_REAL, p.DSCRIPTION FROM FINANCES.dbo.PAYROLL p
LEFT JOIN  tSQL.dbo.EMPLOYEES e 
ON e.EMPLOYEE_ID=p.EMPLOYEE_ID 
WHERE SALARY_REAL &gt SALARY
</pre></code>
**Output :**
| EMPLOYEE\_ID | Name           | MONTH\_PAID | YEAR\_PAID | SALARY | SALARY\_REAL | DSCRIPTION |
| ------------ | -------------- | ----------- | ---------- | ------ | ------------ | ---------- |
| 138          | Stephen Stiles | 1           | 2020       | 10     | 3200         |            |
| 138          | Stephen Stiles | 2           | 2020       | 10     | 3200         |            |
| 138          | Stephen Stiles | 3           | 2020       | 10     | 3200         |            |
| 138          | Stephen Stiles | 4           | 2020       | 10     | 3200         |            |
| 138          | Stephen Stiles | 5           | 2020       | 10     | 3200         |            |
| 138          | Stephen Stiles | 6           | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 7           | 2020       | 24000  | 25000        |            |
| 138          | Stephen Stiles | 7           | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 8           | 2020       | 24000  | 25000        |            |
| 138          | Stephen Stiles | 8           | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 9           | 2020       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 9           | 2020       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 9           | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 10          | 2020       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 10          | 2020       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 10          | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 11          | 2020       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 11          | 2020       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 11          | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 12          | 2020       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 12          | 2020       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 12          | 2020       | 10     | 3200         |            |
| 100          | Steven King    | 1           | 2021       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 1           | 2021       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 1           | 2021       | 10     | 3200         |            |
| 100          | Steven King    | 2           | 2021       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 2           | 2021       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 2           | 2021       | 10     | 3200         |            |
| 100          | Steven King    | 4           | 2021       | 24000  | 25000        |            |
| 101          | Neena Kochhar  | 4           | 2021       | 17000  | 18000        |            |
| 138          | Stephen Stiles | 4           | 2021       | 10     | 3200         |            |

6. **Temukan pegawai yang mendapatkan komisi tidak sesuai dengan table HR!**
* Tampilan Employee_Id, Name, Month, Year, Commission_Pct, Commission_real, Commission_difference)
* Hint! GUNAKAN ROUND()
<pre><code>
USE tSQL
GO

SELECT s.employee_id, e.first_name, e.last_name, s.month_paid, s.year_paid, s.commission_real, (e.commission_pct*s.sales) as commission_real, round((s.commission_real-(e.commission_pct*s.sales)),2) as commission_difference
from FINANCES.dbo.SALESCOMMISSION s
join tsql.dbo.EMPLOYEES2 e
ON s.employee_id=e.employee_id
WHERE round((s.commission_real-(e.commission_pct*s.sales)),0)!=0
</pre></code>
**Output :**
| employee\_id | first\_name | last\_name | month\_paid | year\_paid | commission\_real | commission\_real | commission\_difference |
| ------------ | ----------- | ---------- | ----------- | ---------- | ---------------- | ---------------- | ---------------------- |
| 147          | Alberto     | Errazuriz  | 4           | 2020       | 6055,9           | 6255,9           | \-200                  |

7. **Buatlah sebuah tampilan jumlah pendapatan diterima oleh divisi sales pada tahun 2020**
* Tampilan Employee_Id, Nama, Job_Title, Month, Year, Commission_Pct, Salary_real, Commission_Real, Total_THP (Salary_Real + Commission_Real)
* Urutkan berdasarkan EMPLOYEE_ID, MONTH_PAID, YEAR_PAID!
* Hint! GUNAKAN 4 TABLE
<pre><code>
USE tSQL
GO

SELECT p.Employee_Id, e.FIRST_NAME, e.LAST_NAME, e.JOB_ID, p.MONTH_PAID, p.YEAR_PAID, e.Commission_Pct, 
p.Salary_real, s.Commission_Real, (p.Salary_Real + ISNULL(s.COMMISSION_REAL,0)) as TOTAL_THP FROM EMPLOYEES e
LEFT JOIN FINANCES.dbo.PAYROLL p
ON e.Employee_Id=p.Employee_Id
LEFT JOIN FINANCES.dbo.SALESCOMMISSION s 
ON e.Employee_Id=s.Employee_Id AND p.MONTH_PAID=s.MONTH_PAID AND p.YEAR_PAID=s.YEAR_PAID
ORDER BY p.EMPLOYEE_ID
</pre><code>
**Output :**
| Employee\_Id | FIRST\_NAME | LAST\_NAME | JOB\_ID     | MONTH\_PAID | YEAR\_PAID | Commission\_Pct | Salary\_real | Commission\_Real | TOTAL\_THP |
| ------------ | ----------- | ---------- | ----------- | ----------- | ---------- | --------------- | ------------ | ---------------- | ---------- |
| NULL         | William     | Gietz      | AC\_ACCOUNT | NULL        | NULL       | NULL            | NULL         | NULL             | NULL       |
| 100          | Steven      | King       | AD\_PRES    | 4           | 2021       | NULL            | 25000        | NULL             | 25000      |
| 100          | Steven      | King       | AD\_PRES    | 1           | 2021       | NULL            | 25000        | NULL             | 25000      |
| …            | …           | …          | …           | …           | …          | …               | …            | …                | …          |
| 205          | Shelley     | Higgins    | AC\_MGR     | 1           | 2021       | NULL            | 12008        | NULL             | 12008      |
| 205          | Shelley     | Higgins    | AC\_MGR     | 4           | 2021       | NULL            | 12008        | NULL             | 12008      |
