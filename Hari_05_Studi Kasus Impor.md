# STUDI KASUS DATA IMPOR
Database : CASE_IMPOR
- Tabel header PIB : HEADER
- Tabel detail barang : BARANG
- Tabel daftar container : CONTAINER
- Tabel Referensi HS 2 Digit : REF_HS2
- Tabel Referensi Negara Asal : REF_NEGASAL

## Pada tahap perencanaan pengawasan, Anda diminta untuk menyediakan informasi umum berikut:
1. **Daftar jumlah barang, nilai pabean, kode satuan impor berdasarkan HS_CODE yang berasal dari China, urutkan dari jumlah nilai pabean terbesar**
<pre><code>
SELECT HS_CODE
	, SUM(NILAI_PABEAN) JUMLAH_NILAI_PABEAN
	, COUNT(HS_CODE) JUMLAH_BARANG
	, KODE_SATUAN
FROM BARANG B
WHERE NEG_ASAL = 'CN'
GROUP BY HS_CODE, KODE_SATUAN
ORDER BY JUMLAH_NILAI_PABEAN DESC
</pre></code>

2. **10 HS 2 Digit dengan nilai pabean terbesar**
<pre><code>
SELECT TOP 10 A.ID_IMP
	, SUM(B.NILAI_PABEAN) JUMLAH_PABEAN
	, COUNT(ID_IMP) JUMLAH
FROM HEADER A 
LEFT JOIN BARANG B
ON A.NO_PIB = B.NO_PIB
GROUP BY A.ID_IMP
ORDER BY JUMLAH_PABEAN DESC;
</pre></code>

3. **Nilai Pabean per HS 2 Digit.**
- Sertakan keterangan HS 2 Digit dari tabel referensi HS 2 Digit
- Urutkan dari nilai pabean terbesar
<pre><code>
SELECT LEFT(HS_CODE,2)
	, SUM(NILAI_PABEAN) JUMLAH_NILAI_PABEAN
	, KET
FROM BARANG B
LEFT JOIN REF_HS2 ON REF_HS2.HS2 = LEFT(HS_CODE,2)
GROUP BY LEFT(HS_CODE,2), KET
ORDER BY JUMLAH_NILAI_PABEAN DESC
</pre></code>

4. **Nilai Pabean per Negara Asal.**
- Sertakan keterangan negara asal dari tabel referensi negara
- Urutkan dari nilai pabean terbesar
<pre><code>
SELECT B.JALUR
	, COUNT(B.NO_PIB) JUMLAH_PIB
FROM (
SELECT A.*
	, 
	CASE 
		WHEN SUBSTRING(STATUS_JALUR,1,1) = 'H'  THEN 'HIJAU'
		WHEN STATUS_JALUR = 'MK' THEN 'KUNING'
		WHEN STATUS_JALUR = 'RH' THEN 'RUSH HANDLING'
		ELSE 'MERAH' 
		END AS JALUR
FROM HEADER A ) B
GROUP BY B.JALUR;
</pre></code>

5. **Jumlah dokumen impor pada masing-masing jalur.**
- HIJAU (HH, HL, HM, HP, HT)
- KUNING (MK)
- MERAH (MA, MH, MM)
- RUSH HANDLING (RH)
<pre><code>
SELECT A.*
	, A.NILAI_PABEAN/JUMLAH_SATUAN HARGA_SAT
	, B.BATAS_ATAS
	, B.BATAS_BAWAH
FROM BARANG A JOIN 
( 
SELECT HS_CODE
	, KODE_SATUAN
	, AVG((NILAI_PABEAN/JUMLAH_SATUAN )) + STDEV((NILAI_PABEAN/JUMLAH_SATUAN )) BATAS_ATAS
	, AVG((NILAI_PABEAN/JUMLAH_SATUAN )) - STDEV((NILAI_PABEAN/JUMLAH_SATUAN )) BATAS_BAWAH
FROM BARANG
WHERE HS_CODE = '56074900' AND
	KODE_SATUAN = 'KGM'
GROUP BY HS_CODE, KODE_SATUAN ) B
ON A.HS_CODE = B.HS_CODE
AND A.KODE_SATUAN = B.KODE_SATUAN
WHERE A.NILAI_PABEAN/JUMLAH_SATUAN  > B.BATAS_ATAS OR A.NILAI_PABEAN/JUMLAH_SATUAN < B.BATAS_BAWAH;
</pre></code>

## II.	Pada pelaksanaan pengawasan, Anda diminta melakukan pengujian substantif untuk mencari anomali pada nilai pabean per satuan pada kode HS berikut:

| HS\_CODE | KODE\_SATUAN |
| -------- | ------------ |
| 56074900 | KGM          |
| 69072191 | MTK          |
| 73102999 | PCE          |
| 73239310 | PCE          |
| 94038990 | PCE          |

### Asumsi Kasus
Anomali yang dimaksud adalah Harga Satuan (Nilai Pabean / Jumlah Satuan) yang lebih dari nilai Rata-Rata + (2 x Standar Deviasi) atau kurang dari nilai Rata-Rata – (2 x Standar Deviasi) pada kode HS dan kode satuan tsb.

- Diminta :
- Lampirkan Nomor PIB, Tanggal PIB, HS_Code, Kode_Satuan, Harga_Satuan

<pre><code>
SELECT A.*
	, A.NILAI_PABEAN/JUMLAH_SATUAN HARGA_SAT
	, B.BATAS_ATAS
	, B.BATAS_BAWAH
FROM BARANG A JOIN 
( 
SELECT HS_CODE
	, KODE_SATUAN
	, AVG((NILAI_PABEAN/JUMLAH_SATUAN )) + STDEV((NILAI_PABEAN/JUMLAH_SATUAN )) BATAS_ATAS
	, AVG((NILAI_PABEAN/JUMLAH_SATUAN )) - STDEV((NILAI_PABEAN/JUMLAH_SATUAN )) BATAS_BAWAH
FROM BARANG
WHERE HS_CODE = '56074900' AND
	KODE_SATUAN = 'KGM'
GROUP BY HS_CODE, KODE_SATUAN ) B
ON A.HS_CODE = B.HS_CODE
AND A.KODE_SATUAN = B.KODE_SATUAN
WHERE A.NILAI_PABEAN/JUMLAH_SATUAN  > B.BATAS_ATAS OR A.NILAI_PABEAN/JUMLAH_SATUAN < B.BATAS_BAWAH;
</pre></code>
