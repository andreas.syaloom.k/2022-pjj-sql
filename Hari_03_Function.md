# Hari 3: Function
***

1.	Tahun 2023, anggaran gaji perusahaan tahun tersebut adalah 3.450.000. Karena tuntutan serikat pekerja, perusahaan ingin menaikkan gaji masing-masing pegawai dengan presentase sebanyak masa kerja pegawai dalam tahun (misal masa kerja 5 tahun kenaikan gajinya 5%). Apakah perusahaan punya 
cukup anggaran untuk menaikkan gaji pegawai sesuai perhitungan tersebut?
```
--kenaikan per pegawai
select FIRST_NAME, LAST_NAME, HIRE_DATE, SALARY*12 SALARY_TAHUNAN, DATEDIFF(YEAR,HIRE_DATE,GETDATE()) MASA_KERJA, DATEDIFF(YEAR,HIRE_DATE,GETDATE())/100 KENAIKAN_GAJI, SALARY*12*DATEDIFF(YEAR,HIRE_DATE,GETDATE())/100 KENAIKAN FROM EMPLOYEES-- total kenaikan
--kenaikan total
SELECT SUM(GAJI_BARU) AS TOTAL_KENAIKAN
FROM (select FIRST_NAME, LAST_NAME, HIRE_DATE, SALARY*12 SALARY_TAHUNAN, DATEDIFF(YEAR,HIRE_DATE,GETDATE()) MASA_KERJA, DATEDIFF(YEAR,HIRE_DATE,GETDATE())/100 KENAIKAN_GAJI, SALARY*12*DATEDIFF(YEAR,HIRE_DATE,GETDATE())/100 GAJI_BARU FROM EMPLOYEES) AS TABEL_BARU
```
**Output:**
| FIRST\_NAME | LAST\_NAME  | HIRE\_DATE | SALARY\_TAHUNAN | MASA\_KERJA | KENAIKAN\_GAJI | KENAIKAN      |
| ----------- | ----------- | ---------- | --------------- | ----------- | -------------- | ------------- |
| Steven      | King        | 17/06/2003 | 288000          | 19          | 0              | 54720.000000  |
| Neena       | Kochhar     | 21/09/2005 | 204000          | 17          | 0              | 34680.000000  |
| Lex         | De Haan     | 13/01/2001 | 204000          | 21          | 0              | 42840.000000  |
| Alexander   | Hunold      | 03/01/2006 | 108000          | 16          | 0              | 17280.000000  |
| Bruce       | Ernst       | 21/05/2007 | 72000           | 15          | 0              | 10800.000000  |
| David       | Austin      | 25/06/2005 | 57600           | 17          | 0              | 9792.000000   |
| Valli       | Pataballa   | 05/02/2006 | 57600           | 16          | 0              | 9216.000000   |
| Diana       | Lorentz     | 07/02/2007 | 50400           | 15          | 0              | 7560.000000   |
| Nancy       | Greenberg   | 17/08/2002 | 144096          | 20          | 0              | 28819.200000  |
| Daniel      | Faviet      | 16/08/2002 | 108000          | 20          | 0              | 21600.000000  |
| John        | Chen        | 28/09/2005 | 98400           | 17          | 0              | 16728.000000  |
| Ismael      | Sciarra     | 30/09/2005 | 92400           | 17          | 0              | 15708.000000  |
| Jose Manuel | Urman       | 07/03/2006 | 93600           | 16          | 0              | 14976.000000  |
| Luis        | Popp        | 07/12/2007 | 82800           | 15          | 0              | 12420.000000  |
| Den         | Raphaely    | 07/12/2002 | 132000          | 20          | 0              | 26400.000000  |
| Alexander   | Khoo        | 18/05/2003 | 37200           | 19          | 0              | 7068.000000   |
| Shelli      | Baida       | 24/12/2005 | 34800           | 17          | 0              | 5916.000000   |
| Sigal       | Tobias      | 24/07/2005 | 33600           | 17          | 0              | 5712.000000   |
| Guy         | Himuro      | 15/11/2006 | 31200           | 16          | 0              | 4992.000000   |
| Karen       | Colmenares  | 10/08/2007 | 30000           | 15          | 0              | 4500.000000   |
| Matthew     | Weiss       | 18/07/2004 | 96000           | 18          | 0              | 17280.000000  |
| Adam        | Fripp       | 10/04/2005 | 98400           | 17          | 0              | 16728.000000  |
| Payam       | Kaufling    | 01/05/2003 | 94800           | 19          | 0              | 18012.000000  |
| Shanta      | Vollman     | 10/10/2005 | 78000           | 17          | 0              | 13260.000000  |
| Kevin       | Mourgos     | 16/11/2007 | 69600           | 15          | 0              | 10440.000000  |
| Julia       | Nayer       | 16/07/2005 | 38400           | 17          | 0              | 6528.000000   |
| Irene       | Mikkilineni | 28/09/2006 | 32400           | 16          | 0              | 5184.000000   |
| James       | Landry      | 14/01/2007 | 28800           | 15          | 0              | 4320.000000   |
| Steven      | Markle      | 08/03/2008 | 26400           | 14          | 0              | 3696.000000   |
| Laura       | Bissot      | 20/08/2005 | 39600           | 17          | 0              | 6732.000000   |
| Mozhe       | Atkinson    | 30/10/2005 | 1188000         | 17          | 0              | 201960.000000 |
| James       | Marlow      | 16/02/2005 | 30000           | 17          | 0              | 5100.000000   |
| TJ          | Olson       | 10/04/2007 | 25200           | 15          | 0              | 3780.000000   |
| Jason       | Mallin      | 14/06/2004 | 39600           | 18          | 0              | 7128.000000   |
| Michael     | Rogers      | 26/08/2006 | 34800           | 16          | 0              | 5568.000000   |
| Ki          | Gee         | 12/12/2007 | 28800           | 15          | 0              | 4320.000000   |
| Hazel       | Philtanker  | 06/02/2008 | 26400           | 14          | 0              | 3696.000000   |
| Renske      | Ladwig      | 14/07/2003 | 43200           | 19          | 0              | 8208.000000   |
| Stephen     | Stiles      | 26/10/2005 | 120             | 17          | 0              | 20.400000     |
| John        | Seo         | 12/02/2006 | 32400           | 16          | 0              | 5184.000000   |
| Joshua      | Patel       | 06/04/2006 | 30000           | 16          | 0              | 4800.000000   |
| Trenna      | Rajs        | 17/10/2003 | 42000           | 19          | 0              | 7980.000000   |
| Curtis      | Davies      | 29/01/2005 | 37200           | 17          | 0              | 6324.000000   |
| Randall     | Matos       | 15/03/2006 | 31200           | 16          | 0              | 4992.000000   |
| Peter       | Vargas      | 09/07/2006 | 30000           | 16          | 0              | 4800.000000   |
| John        | Russell     | 01/10/2004 | 168000          | 18          | 0              | 30240.000000  |
| Karen       | Partners    | 05/01/2005 | 162000          | 17          | 0              | 27540.000000  |
| Alberto     | Errazuriz   | 10/03/2005 | 144000          | 17          | 0              | 24480.000000  |
| Gerald      | Cambrault   | 15/10/2007 | 132000          | 15          | 0              | 19800.000000  |
| Eleni       | Zlotkey     | 29/01/2008 | 126000          | 14          | 0              | 17640.000000  |
| Peter       | Tucker      | 30/01/2005 | 120000          | 17          | 0              | 20400.000000  |
| David       | Bernstein   | 24/03/2005 | 114000          | 17          | 0              | 19380.000000  |
| Peter       | Hall        | 20/08/2005 | 108000          | 17          | 0              | 18360.000000  |
| Christopher | Olsen       | 30/03/2006 | 96000           | 16          | 0              | 15360.000000  |
| Nanette     | Cambrault   | 09/12/2006 | 90000           | 16          | 0              | 14400.000000  |
| Oliver      | Tuvault     | 23/11/2007 | 84000           | 15          | 0              | 12600.000000  |
| Janette     | King        | 30/01/2004 | 120000          | 18          | 0              | 21600.000000  |
| Patrick     | Sully       | 04/03/2004 | 114000          | 18          | 0              | 20520.000000  |
| Allan       | McEwen      | 01/08/2004 | 108000          | 18          | 0              | 19440.000000  |
| Lindsey     | Smith       | 10/03/2005 | 96000           | 17          | 0              | 16320.000000  |
| Louise      | Doran       | 15/12/2005 | 90000           | 17          | 0              | 15300.000000  |
| Sarath      | Sewall      | 03/11/2006 | 84000           | 16          | 0              | 13440.000000  |
| Clara       | Vishney     | 11/11/2005 | 126000          | 17          | 0              | 21420.000000  |
| Danielle    | Greene      | 19/03/2007 | 114000          | 15          | 0              | 17100.000000  |
| Mattea      | Marvins     | 24/01/2008 | 86400           | 14          | 0              | 12096.000000  |
| David       | Lee         | 23/02/2008 | 81600           | 14          | 0              | 11424.000000  |
| Sundar      | Ande        | 24/03/2008 | 76800           | 14          | 0              | 10752.000000  |
| Amit        | Banda       | 21/04/2008 | 74400           | 14          | 0              | 10416.000000  |
| Lisa        | Ozer        | 11/03/2005 | 138000          | 17          | 0              | 23460.000000  |
| Harrison    | Bloom       | 23/03/2006 | 120000          | 16          | 0              | 19200.000000  |
| Tayler      | Fox         | 24/01/2006 | 115200          | 16          | 0              | 18432.000000  |
| William     | Smith       | 23/02/2007 | 88800           | 15          | 0              | 13320.000000  |
| Elizabeth   | Bates       | 24/03/2007 | 87600           | 15          | 0              | 13140.000000  |
| Sundita     | Kumar       | 21/04/2008 | 73200           | 14          | 0              | 10248.000000  |
| Ellen       | Abel        | 11/05/2004 | 132000          | 18          | 0              | 23760.000000  |
| Alyssa      | Hutton      | 19/03/2005 | 105600          | 17          | 0              | 17952.000000  |
| Jonathon    | Taylor      | 24/03/2006 | 103200          | 16          | 0              | 16512.000000  |
| Jack        | Livingston  | 23/04/2006 | 100800          | 16          | 0              | 16128.000000  |
| Kimberely   | Grant       | 24/05/2007 | 84000           | 15          | 0              | 12600.000000  |
| Charles     | Johnson     | 04/01/2008 | 74400           | 14          | 0              | 10416.000000  |
| Winston     | Taylor      | 24/01/2006 | 38400           | 16          | 0              | 6144.000000   |
| Jean        | Fleaur      | 23/02/2006 | 37200           | 16          | 0              | 5952.000000   |
| Martha      | Sullivan    | 21/06/2007 | 30000           | 15          | 0              | 4500.000000   |
| Girard      | Geoni       | 03/02/2008 | 33600           | 14          | 0              | 4704.000000   |
| Nandita     | Sarchand    | 27/01/2004 | 50400           | 18          | 0              | 9072.000000   |
| Alexis      | Bull        | 20/02/2005 | 49200           | 17          | 0              | 8364.000000   |
| Julia       | Dellinger   | 24/06/2006 | 40800           | 16          | 0              | 6528.000000   |
| Anthony     | Cabrio      | 07/02/2007 | 36000           | 15          | 0              | 5400.000000   |
| Kelly       | Chung       | 14/06/2005 | 45600           | 17          | 0              | 7752.000000   |
| Jennifer    | Dilly       | 13/08/2005 | 43200           | 17          | 0              | 7344.000000   |
| Timothy     | Gates       | 11/07/2006 | 34800           | 16          | 0              | 5568.000000   |
| Randall     | Perkins     | 19/12/2007 | 30000           | 15          | 0              | 4500.000000   |
| Sarah       | Bell        | 04/02/2004 | 48000           | 18          | 0              | 8640.000000   |
| Britney     | Everett     | 03/03/2005 | 46800           | 17          | 0              | 7956.000000   |
| Samuel      | McCain      | 01/07/2006 | 38400           | 16          | 0              | 6144.000000   |
| Vance       | Jones       | 17/03/2007 | 33600           | 15          | 0              | 5040.000000   |
| Alana       | Walsh       | 24/04/2006 | 37200           | 16          | 0              | 5952.000000   |
| Kevin       | Feeney      | 23/05/2006 | 36000           | 16          | 0              | 5760.000000   |
| Donald      | OConnell    | 21/06/2007 | 31200           | 15          | 0              | 4680.000000   |
| Douglas     | Grant       | 13/01/2008 | 31200           | 14          | 0              | 4368.000000   |
| Jennifer    | Whalen      | 17/09/2003 | 52800           | 19          | 0              | 10032.000000  |
| Michael     | Hartstein   | 17/02/2004 | 156000          | 18          | 0              | 28080.000000  |
| Pat         | Fay         | 17/08/2005 | 72000           | 17          | 0              | 12240.000000  |
| Susan       | Mavris      | 07/06/2002 | 78000           | 20          | 0              | 15600.000000  |
| Hermann     | Baer        | 07/06/2002 | 120000          | 20          | 0              | 24000.000000  |
| Shelley     | Higgins     | 07/06/2002 | 144096          | 20          | 0              | 28819.200000  |
| William     | Gietz       | 07/06/2002 | 99600           | 20          | 0              | 19920.000000  |

2.	Berapa orang pegawai dengan jabatan (job_id) berakhiran _clerk yang gajinya di bawah rata-rata dibanding gaji seluruh pegawai di perusahaan?
```
-- Untuk melihat listnya
SELECT  EMPLOYEE_ID, FIRST_NAME, LAST_NAME, JOB_ID, SALARY FROM EMPLOYEES
WHERE JOB_ID LIKE '%CLERK' AND SALARY<(SELECT AVG(SALARY) FROM EMPLOYEES)
--untuk menghitung rownya
SELECT COUNT(EMPLOYEE_ID) FROM(SELECT  EMPLOYEE_ID, FIRST_NAME, LAST_NAME, JOB_ID, SALARY FROM EMPLOYEES
WHERE JOB_ID LIKE '%CLERK' AND SALARY<(SELECT AVG(SALARY) FROM EMPLOYEES)) AS TABEL_PERANTARA
```
**Output:** 44 records

| EMPLOYEE\_ID | FIRST\_NAME | LAST\_NAME  | JOB\_ID   | SALARY |
| ------------ | ----------- | ----------- | --------- | ------ |
| 115          | Alexander   | Khoo        | PU\_CLERK | 3100   |
| 116          | Shelli      | Baida       | PU\_CLERK | 2900   |
| 117          | Sigal       | Tobias      | PU\_CLERK | 2800   |
| 118          | Guy         | Himuro      | PU\_CLERK | 2600   |
| 119          | Karen       | Colmenares  | PU\_CLERK | 2500   |
| 125          | Julia       | Nayer       | ST\_CLERK | 3200   |
| 126          | Irene       | Mikkilineni | ST\_CLERK | 2700   |
| 127          | James       | Landry      | ST\_CLERK | 2400   |
| 128          | Steven      | Markle      | ST\_CLERK | 2200   |
| 129          | Laura       | Bissot      | ST\_CLERK | 3300   |
| 131          | James       | Marlow      | ST\_CLERK | 2500   |
| 132          | TJ          | Olson       | ST\_CLERK | 2100   |
| 133          | Jason       | Mallin      | ST\_CLERK | 3300   |
| 134          | Michael     | Rogers      | ST\_CLERK | 2900   |
| 135          | Ki          | Gee         | ST\_CLERK | 2400   |
| 136          | Hazel       | Philtanker  | ST\_CLERK | 2200   |
| 137          | Renske      | Ladwig      | ST\_CLERK | 3600   |
| 138          | Stephen     | Stiles      | ST\_CLERK | 10     |
| 139          | John        | Seo         | ST\_CLERK | 2700   |
| 140          | Joshua      | Patel       | ST\_CLERK | 2500   |
| 141          | Trenna      | Rajs        | ST\_CLERK | 3500   |
| 142          | Curtis      | Davies      | ST\_CLERK | 3100   |
| 143          | Randall     | Matos       | ST\_CLERK | 2600   |
| 144          | Peter       | Vargas      | ST\_CLERK | 2500   |
| 180          | Winston     | Taylor      | SH\_CLERK | 3200   |
| 181          | Jean        | Fleaur      | SH\_CLERK | 3100   |
| 182          | Martha      | Sullivan    | SH\_CLERK | 2500   |
| 183          | Girard      | Geoni       | SH\_CLERK | 2800   |
| 184          | Nandita     | Sarchand    | SH\_CLERK | 4200   |
| 185          | Alexis      | Bull        | SH\_CLERK | 4100   |
| 186          | Julia       | Dellinger   | SH\_CLERK | 3400   |
| 187          | Anthony     | Cabrio      | SH\_CLERK | 3000   |
| 188          | Kelly       | Chung       | SH\_CLERK | 3800   |
| 189          | Jennifer    | Dilly       | SH\_CLERK | 3600   |
| 190          | Timothy     | Gates       | SH\_CLERK | 2900   |
| 191          | Randall     | Perkins     | SH\_CLERK | 2500   |
| 192          | Sarah       | Bell        | SH\_CLERK | 4000   |
| 193          | Britney     | Everett     | SH\_CLERK | 3900   |
| 194          | Samuel      | McCain      | SH\_CLERK | 3200   |
| 195          | Vance       | Jones       | SH\_CLERK | 2800   |
| 196          | Alana       | Walsh       | SH\_CLERK | 3100   |
| 197          | Kevin       | Feeney      | SH\_CLERK | 3000   |
| 198          | Donald      | OConnell    | SH\_CLERK | 2600   |
| 199          | Douglas     | Grant       | SH\_CLERK | 2600   |
3.	Sebutkan 3 JOB_ID yang memiliki maksimum gaji terbesar di perusahaan berdasarkan gaji actual (salary) pada table EMPLOYEES.
```
SELECT TOP 3 JOB_ID, MAX(SALARY) AS SALARYMAX FROM EMPLOYEES
GROUP BY JOB_ID
ORDER BY MAX(SALARY) DESC
```
**Output :**
| JOB\_ID   | SALARYMAX |
| --------- | --------- |
| ST\_CLERK | 99000     |
| AD\_PRES  | 24000     |
| AD\_VP    | 17000     |

4.	Sebutkan 3 manager_id dengan pegawai terbanyak, dan berapa masing-masing rata-rata gaji pegawai di tim manajer tersebut.
```
SELECT TOP 3 MANAGER_ID, COUNT(EMPLOYEE_ID)AS JUMLAH_KARYAWAN, AVG(SALARY)AS RATA_RATA_GAJI  FROM EMPLOYEES
GROUP BY MANAGER_ID
```
**Output:**
| MANAGER\_ID | JUMLAH\_KARYAWAN | RATA\_RATA\_GAJI |
| ----------- | ---------------- | ---------------- |
| NULL        | 1                | 24000.000000     |
| 100         | 14               | 11100.000000     |
| 101         | 5                | 8983.200000      |
